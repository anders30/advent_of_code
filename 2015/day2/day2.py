# Written for Python 2.7 because my computer is that old.

def smallest_val(num_iter):
  ''' Given an iterable of int-ish values, find the minimum.
  It is assumed that all values are > 0.
  Used for Advent of Code, year 2015, day 2.
  '''
  min = num_iter[0]
  for num in num_iter:
    if num < min:
      min = num

  return min

def line_to_nums(line):
  '''
  line_to_nums(str) -> list[int]
  Given a line of input as presented on Day 2, conver the line
  to a list of int-ish values.
  Example str: '2x3x4' -> [2, 3, 4]
  '''
  num_strs = line.split('x')
  return [int(num) for num in num_strs]

def surface_areas(nums):
  '''
  Given a list of numbers representing, l, w, and h, return a list:
  [2*l*w, 2*w*h, 2*h*l]
  '''
  l = nums[0]
  w = nums[1]
  h = nums[2]
  return [2 * l * w, 2 * w * h, 2 * h * l]

def tot_sq_ft(line):
  '''
  tot_sq_ft(str) -> list[int]
  Given a line of input from Advent of Code year 2015 day 2,
  calculate the square feet of wrapping paper required plus the slack
  desired by the elves.
  '''
  nums = line_to_nums(line)
  sr_areas = surface_areas(nums)
  slack = smallest_val(sr_areas) / 2
  #print str(sr_areas), str(slack)
  tot = 0
  for area in sr_areas:
    tot = tot + area 
  tot = tot + slack
  return tot

def volume(nums):
  '''
  volume(list[int]) -> int
  Where the input list has 3 values, len, wid, ht.
  Return value is len * wid * ht.
  '''
  return nums[0] * nums[1] * nums[2]

def smallest_perimeter(nums):
  '''
  smallest_perimeter(list[int]) -> int
  Where the input list has three values and the output value
  is essentially 2 * (min1 + min2) where min1 is the smallest
  of the three values and min2 is the second smallest.
  '''
  l, w, h = nums
  perims = [l+w, w+h, h+l]
  min = smallest_val(perims)

  print "Min perim: " + str(min)
  return 2 * min

def smallest_perimeter_orig(nums):
  '''
  smallest_perimeter(list[int]) -> int
  Where the input list has three values and the output value
  is essentially 2 * (min1 + min2) where min1 is the smallest
  of the three values and min2 is the second smallest.
  XXX - using this function gets a too high answer!
  '''
  min = smallest_val(nums)
  min2 = nums[0]

  if min2 == min:
    min2 = nums[1]

  for num in nums:
    if num < min2 and num != min:
      min2 = num

  print "Mins: " + str(min) + ", " + str(min2)
  return 2 * (min + min2)

def ribbon_length(line):
  '''
  ribbon_length(str) -> int
  Given a text line of the form 'lxwxh', where l, w, and h represent
  integer values,
  calculate the smallest perimeter of any one face of the cube represented
  by the line of text plus the volume.
  '''
  print line[:-1]
  nums = line_to_nums(line)
  perim = smallest_perimeter(nums)
  vol = volume(nums)
  print "P: " + str(perim)
  print "V: " + str(vol)
  print "T: " + str(vol + perim)
  return perim + vol


def puzz2_main():
  import fileinput
  tot = 0
  for line in fileinput.input():
    tot = tot + ribbon_length(line)
    #print str(tot_sq_ft(line))

  print "Total is " + str(tot)

def puzz1_main():
  import fileinput
  tot = 0
  for line in fileinput.input():
    tot = tot + tot_sq_ft(line)
    #print str(tot_sq_ft(line))

  print "Total is " + str(tot)

if __name__ == "__main__":
  #puzz1_main()
  puzz2_main()
  print ""

# Unit tests below
#test1 = '2x3x4'
#total = tot_sq_ft(test1)
#print str(total)
#rib = ribbon_length(test1)
#print str(rib)

#test2 = '1x1x10'
#total = tot_sq_ft(test2)
#print str(total)
#rib = ribbon_length(test2)
#print str(rib)
