# Advent of Code, 2015, Day 3

#def calc_houses(line):
#  curr_pos = [0, 0] # infinite grid, start doesn't matter
#  history = [curr_pos]
#  houses = 1 # Always visited to house at starting point
#
#  for move in line:
#    if move == '^':
#      curr_pos = [curr_pos[0], curr_pos[1] + 1]
#    elif move == '>':
#      curr_pos = [curr_pos[0] + 1, curr_pos[1]]
#    elif move == 'v':
#      curr_pos = [curr_pos[0], curr_pos[1] - 1]
#    elif move == '<':
#      curr_pos = [curr_pos[0] - 1, curr_pos[1]]
#
#    #print str(curr_pos in history)
#
#    if curr_pos not in history:
#      houses = houses + 1
#      history.append(curr_pos)
#
#  return houses
#
#if __name__ == "__main__":
#  import fileinput
#  for line in fileinput.input():
#   print str(calc_houses(line))

def move_pos(mv, orig_pos):
  new_pos = orig_pos
  if mv == '^':
    new_pos = (orig_pos[0], orig_pos[1] + 1)
  elif mv == '>':
    new_pos = (orig_pos[0] + 1, orig_pos[1])
  elif mv == 'v':
    new_pos = (orig_pos[0], orig_pos[1] - 1)
  elif mv == '<':
    new_pos = (orig_pos[0] - 1, orig_pos[1])

  return new_pos


def calc_houses2(line):
  santa_pos = (0, 0) # infinite grid, start doesn't matter
  robo_pos = (0, 0)
  history = { santa_pos: True }
  move_santa = True
  moves = 0

  for move in line:
    moves = moves + 1
    if move_santa:
      santa_pos = move_pos(move, santa_pos)
      history[santa_pos] = True
      move_santa = False
    else:
      robo_pos = move_pos(move, robo_pos)
      history[robo_pos] = True
      move_santa = True

  return len(history)

if __name__ == "__main__":
  line = open('input.txt').readline()
  print str(calc_houses2(line))

## Puzz 2 tests:
test1 = '^v' # 3
print str(calc_houses2(test1))
test2 = '^>v<' # 3
print str(calc_houses2(test2))
test3 = '^v^v^v^v^v' # should be 11
print str(calc_houses2(test3))

# general alg
# for move in moves: # moves is just the line of text
#   if move is north:
#     curr_pos = move_north(curr_pos)
#   if move is south:
#   ... etc ...
#   if curr_pos not in history:
#     houses_visited = houses_visited + 1
#     history.append(curr_pos)



# Unit tests below
#test1 = '>' # Should be 2
#print str(calc_houses(test1))


#test2 = '^>v<' # should be 4 (5 total but one dupe)
#print str(calc_houses(test2))


#test3 = '^v^v^v^v^v' # should be 2
#print str(calc_houses(test3))
